<?php
class CheckoutAPI_PaymentModule_IndexController extends Mage_Core_Controller_Front_Action
{
    const PAYMENT_METHOD_CODE = 'paymentmodule';

    protected function _initProfile($profileId)
    {

        $profile = Mage::getModel('aw_sarp2/profile');
        if (!$profileId ) {
            throw new Exception('Profile ID has not been specified');
        }
        $profile->loadByReferenceId($profileId);
        Mage::register('current_profile', $profile);
        return $profile;
    }



    public function  indexAction()
    {

        /** @var SimpleXMLElement  $respond */
        $respond = $this->_handleRespond();



        if( $respond && $respond['transaction']['status']['responsecode'] == 0) {
            $shopper = $respond['transaction']['shopper'] ;
            $profile =  $this->_initProfile($shopper['recurring']['recurring_token']);
            if ($profile->getId()) {

                Mage::getModel('paymentmodule/engine_paymentModule_payment_rpn')->process($profile, $respond);

            } else {
                $profile->synchronizeWithEngine();
            }
            $this->getResponse()->setBody("<result><responsecode>0</responsecode></result>");
        }else  {
            $this->getResponse()->setBody("<result><responsecode>1</responsecode></result>");
          if($respond) {
              Mage::log($respond->status->error_code_tag);
          }
        }
    }

    private function _handleRespond()
    {
        $xmlRaw = file_get_contents('php://input');
       // $xmlRaw = $this->_simulateXml();

        if(!$xmlRaw) {
            return false;
        }

        $xml = new SimpleXMLElement($xmlRaw);

        $xml   = simplexml_load_string($xmlRaw, 'SimpleXMLElement', LIBXML_NOCDATA);;
        $toArray = json_decode(json_encode((array)$xml), TRUE);
        Mage::log($xmlRaw, null, 'rpn-subscription.log');
        return $toArray;
    }

    public function getProfile()
    {
        $profile = Mage::registry('current_profile');

        if(!Mage::registry('current_profile')) {
            $profile = Mage::getModel('aw_sarp2/profile');
        }

        Mage::register('current_profile', $profile);

        return $profile;
    }

    private function _simulateXml()
    {
//        $xmlS ='<rpnmessage type="payment">
//   <merchant>
//     <merchantid>TestMID </merchantid>
//   </merchant>
//   <transaction>
//     <details>
//       <tranid>35822049</tranid>
//       <transactiondate>10/24/2014 04:16:30 PM</transactiondate>
//       <transactiontype>Recurring</transactiontype>
//       <trackid>42</trackid>
//       <currencycode>840</currencycode>
//       <amount>1.00</amount>
//       <action>Purchase</action>
//     </details>
//     <status>
//       <result>Successful</result>
//       <responsecode>1</responsecode>
//       <error_code_tag />
//     </status>
//     <other>
//       <billing_details>
//          <bill_expmonth>6</bill_expmonth>
//          <bill_expyear>2017</bill_expyear>
//          <bill_cvv2>956</bill_cvv2>
//          <bill_cc>4543474002249996</bill_cc>
//          <bill_address>test</bill_address>
//          <bill_postal>20000</bill_postal>
//          <bill_phone>4654564654</bill_phone>
//          <bill_city>test</bill_city>
//          <bill_state>Alaska</bill_state>
//          <bill_email>dhiraj_gangoosirdar_checkout@gmail.com</bill_email>
//          <bill_country>US</bill_country>
//          <bill_amount>200</bill_amount>
//       </billing_details>
//       <shiping_details>
//         <ship_address>test</ship_address>
//         <ship_email>dhiraj_gangoosirdar_checkout@gmail.com</ship_email>
//         <ship_postal>20000</ship_postal>
//         <ship_type>Flat Rate - Fixed</ship_type>
//         <ship_city>test</ship_city>
//         <ship_state>Alaska</ship_state>
//         <ship_phone>4654564654</ship_phone>
//         <ship_country>US</ship_country>
//       </shiping_details>
//     </other>
//     <shopper>
//       <shopper_profile>f95ddfeb-c3a3-433a-ac11-5e06bc3d4575</shopper_profile>
//       <customer_token>da15260e-5999-4caf-8357-57051574d0ff</customer_token>
//       <merchantcustomerid>C002</merchantcustomerid>
//       <recurring type="recurring">
//         <recurring_token>6d75b9b3-6dc4-49ac-935d-54000b6331e6</recurring_token>
//         <interval>1</interval>
//         <intervaltype>DAY</intervaltype>
//         <intervaltype>DAY</intervaltype>
//           <numberleft>1</numberleft>
//         <status>1</status>
//       </recurring>
//     </shopper>
//   </transaction>
// </rpnmessage>';
//        return $xmlS;
    }
}