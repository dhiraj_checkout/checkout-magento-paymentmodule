<?php
class CheckoutAPI_PaymentModule_Model_Engine_PaymentModule_Payment_Rpn
{
    /**
     * @var AW_Sarp2_Model_Profile
     */
    protected $_recurringProfile;

    /**
     * @param AW_Sarp2_Model_Profile $p
     * @param array $respond
     */
    public function process(AW_Sarp2_Model_Profile $p, $respond)
    {
        $this->_recurringProfile = $p;
        $price = $respond['transaction']['details']['amount'];
        $productItemInfo = new Varien_Object;
        $paymentNumber = $respond['transaction']['shopper']['recurring']['interval'];
        $initialDetails = $this->_recurringProfile->getInitialDetails();
        if (
            $initialDetails['subscription']['type']['trial_is_enabled']
            && ($paymentNumber <= $initialDetails['subscription']['type']['trial_number_of_occurrences'])
        ) {
            $productItemInfo->setPaymentType(Mage_Sales_Model_Recurring_Profile::PAYMENT_TYPE_TRIAL);
        } elseif (
            $paymentNumber <= $initialDetails['subscription']['type']['period_number_of_occurrences']
            || $initialDetails['subscription']['type']['period_is_infinite']
        ) {
            $productItemInfo->setPaymentType(Mage_Sales_Model_Recurring_Profile::PAYMENT_TYPE_REGULAR);
        } else {
            $p->synchronizeWithEngine();
            exit;
        }
        //$productItemInfo->setTaxAmount(2);
        $productItemInfo->setShippingAmount($initialDetails['shipping_amount']);
        $productItemInfo->setPrice($price);
        $productItemInfo->setWeeeTaxApplied(serialize(array()));

        $order = $this->_recurringProfile->createOrder($productItemInfo);
        $items = $order->getItemsCollection();
        $order->setData(null);
        foreach($items as $item) {
            $item->setWeeeTaxApplied(serialize(array()))  ;
            $order->addItem($item);
        }

        $payment = $order->getPayment();

        $payment->setTransactionId($respond['transaction']['details']['trackid'])
            ->setPreparedMessage($respond['transaction']['status']['result'])
            ->setIsTransactionClosed(0);
        $order->save();
        $this->_recurringProfile->addOrderRelation($order->getId());

        $payment->registerCaptureNotification($order->getBaseGrandTotal());
        $order->save();

        $p->synchronizeWithEngine();

        // notify customer
        if ($invoice = $payment->getCreatedInvoice()) {
            $message = Mage::helper('paygate')->__('Notified customer about invoice #%s.', $invoice->getIncrementId());
            $order->sendNewOrderEmail()->addStatusHistoryComment($message)
                ->setIsCustomerNotified(true)
                ->save()
            ;
        }
    }
}