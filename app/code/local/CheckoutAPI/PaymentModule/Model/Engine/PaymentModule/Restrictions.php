<?php

class CheckoutAPI_PaymentModule_Model_Engine_PaymentModule_Restrictions extends  AW_Sarp2_Model_Engine_Authorizenet_Restrictions
{
    /**
     * @param string $currentStatus
     *
     * @return array = update|activate|suspend|cancel
     */
    public function getAvailableSubscriptionOperations($currentStatus)
    {
        switch ($currentStatus) {
            case 'active':
            case 'suspended':
                return array('update', 'cancel');
            case 'expired':
            case 'canceled':
            case 'terminated':
                return array();
            default:
                return array();
        }
    }
    /**
     * @return string
     */
    public function getStartDateFormat()
    {
        //ISO format
        return 'MM-dd-yyyyThh:mm:ss';
    }

    /**
     * @return array
     */
    public function getAvailableUnitOfTime()
    {
        return array('days', 'months');
    }


    public function validateInitialAmount($amount)
    {
        throw new AW_Sarp2_Model_Engine_PaymentRestrictionsException(
            "Incorrect initial amount. Amount not available by Checkout.com"
        );
    }

    public function validateInterval($unit, $length)
    {


        return $this;
    }
}