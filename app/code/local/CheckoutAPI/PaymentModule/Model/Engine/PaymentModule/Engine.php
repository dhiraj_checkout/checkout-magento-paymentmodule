<?php
class CheckoutAPI_PaymentModule_Model_Engine_PaymentModule_Engine extends AW_Sarp2_Model_Engine_Authorizenet_Engine
{
    /**
     * Engine code
     *
     * @var string
     */
    protected $_engineCode = 'paymentmodule';

    /**
     * Payment restrictions for engine
     *
     * @var AW_Sarp2_Model_Engine_Paypal_Restrictions
     */
    protected $_paymentRestrictions;

    /**
     * Engine of service
     *
     * @var AW_Sarp2_Model_Engine_Paypal_Service
     */
    protected $_service;


//
//    protected $_mapRestrictionsToService = array(
//
//    );
//
//    protected $_mapRestrictionsToProfile = array(
//
//    );
//
//    protected $_responseMapRestrictionsToService = array(
//
//    );


    protected $_mapRestrictionsToService = array(

        'bill_currencycode'         => 'details/currency_code',
        'bill_cardholder'           => 'details/payment/cc_owner',
        'trackid'                   => 'details/order_info/entity_id',
        'bill_currencycode'         => 'details/currency_code',
        'bill_expmonth'             => 'details/payment/cc_exp_month',
        'bill_expyear'              => 'details/payment/cc_exp_year',
        'bill_cvv2'                 => 'details/payment/cc_cid',
        'bill_cc'                   => 'details/payment/cc_number',
        'bill_address'              => 'details/billing_address/street',
        'bill_postal'               => 'details/billing_address/postcode',
        'bill_phone'               => 'details/billing_address/telephone',
        'bill_city'                 => 'details/billing_address/city',
        'bill_state'                => 'details/billing_address/region',
        'bill_email'                => 'details/billing_address/email',
        'bill_country'              => 'details/billing_address/country_id',

        'bill_phone'                => 'details/billing_address/telephone',
        'bill_fax'                  => 'details/billing_address/fax',
        'ship_address'              => 'details/shipping_address/street',
        'ship_email'                => 'details/shipping_address/email',
        'ship_postal'                => 'details/shipping_address/postcode',
        'ship_type'                => 'details/shipping_address/shipping_method',
        'ship_type'                => 'details/shipping_address/shipping_description',
        'ship_city'                => 'details/shipping_address/city',
        'ship_state'                => 'details/shipping_address/region',
        'ship_phone'                => 'details/shipping_address/telephone',
        'ship_country'                => 'details/shipping_address/country_id',
        'ship_fax'                => 'details/shipping_address/fax',
        'trackid'                => 'details/order_item_info/quote_id',
        'recurring_startdate'    => 'subscription/paymentSchedule/startDate',
        'recurring_amount'    => 'subscription/amount',
        'recurring_number'    => 'BILLINGFREQUENCY',
        'start_date'                => 'PROFILESTARTDATE'

    );

    protected $_mapRestrictionsToProfile = array(
        'start_date'                => 'start_date',
        'bill_currencycode'         => 'details/currency_code',
        'bill_cardholder'           => 'details/payment/cc_owner',
        'trackid'                   => 'details/order_info/entity_id',
        'bill_currencycode'         => 'details/currency_code',
        'bill_expmonth'             => 'details/payment/cc_exp_month',
        'bill_expyear'              => 'details/payment/cc_exp_year',
        'bill_cvv2'                 => 'details/payment/cc_cid',
        'bill_cc'                   => 'details/payment/cc_number',
        'bill_address'              => 'details/billing_address/street',
        'bill_postal'               => 'details/billing_address/postcode',
        'bill_phone'               => 'details/billing_address/telephone',
        'bill_city'                 => 'details/billing_address/city',
        'bill_state'                => 'details/billing_address/region',
        'bill_email'                => 'details/billing_address/email',
        'bill_country'              => 'details/billing_address/country_id',
        'bill_amount'               => 'amount',
        'bill_phone'                => 'details/billing_address/telephone',
        'bill_fax'                  => 'details/billing_address/fax',
        'ship_address'              => 'details/shipping_address/street',
        'ship_email'                => 'details/shipping_address/email',
        'ship_postal'                => 'details/shipping_address/postcode',
        'ship_type'                => 'details/shipping_address/shipping_method',
        'ship_type'                => 'details/shipping_address/shipping_description',
        'ship_city'                => 'details/shipping_address/city',
        'ship_state'                => 'details/shipping_address/region',
        'ship_phone'                => 'details/shipping_address/telephone',
        'ship_country'                => 'details/shipping_address/country_id',
        'ship_fax'                => 'details/shipping_address/fax',
        'status'                        => 'status'
    );


    function __construct()
    {
        /*
         * Initialize class parameters
         */
        $this->_paymentRestrictions = Mage::getSingleton('paymentmodule/engine_paymentModule_restrictions');
        $this->_service = Mage::getSingleton('paymentmodule/engine_paymentModule_service');
    }

    /**
     * @param AW_Sarp2_Model_Profile $p
     *
     * @throws Mage_Core_Exception
     */
    public function createRecurringProfile(AW_Sarp2_Model_Profile $p)
    {
        $this->_initConfigData($p);
        $data = $this->_importDataForValidation($p);

        try {
            $this->getPaymentRestrictionsModel()->validateAll($data);
        } catch (AW_Sarp2_Model_Engine_PaymentRestrictionsException $e) {
            throw new Mage_Core_Exception($e->getMessage());
        }
        $requestData = $this->_importDataForService($p);

        try {
            $quote = Mage::getModel('sales/quote')->load($p->getData('details/order_item_info/quote_id'));
            if (is_null($quote->getId())) {
                throw new Exception('Unable get quote');
            }

            $response = $this->_service->createSubscription($requestData);

            $details = $p->getData('details');
            unset($details['payment']);
            $p->addData(
                array(
                    'reference_id' => $response['recurring_token'],
                    'start_date'   => $data['start_date']->toString(Zend_Date::W3C),
                    'details'      => $details,
                )
            );
        } catch (AW_Sarp2_Model_Engine_Authorizenet_Service_Exception $e) {
            Mage::logException($e);
            $message = Mage::helper('aw_sarp2')->__(
                'Unable create subscription on Checkout.com: %s', $e->getMessage()
            );
            throw new Mage_Core_Exception($message);
        } catch (Exception $e) {
            Mage::logException($e);
            throw new Mage_Core_Exception('Unable create subscription on Checkout.com:');
        }
    }

    /**
     * @param AW_Sarp2_Model_Profile $p
     */
    private function _initConfigData(AW_Sarp2_Model_Profile $p)
    {
        $methodInstance = Mage::helper('payment')->getMethodInstance($p->getData('details/method_code'));
        $methodInstance->setStore($p->getData('details/store_id'));

        $this->_service->setConfigData(
            array(
                'test'       => $methodInstance->getConfigData('test'),
                'username'           =>  Mage::helper('core')->decrypt($methodInstance->getConfigData('user_name')),
                'password'           => Mage::helper('core')->decrypt($methodInstance->getConfigData('password'))
            )
        );
    }

    /**
     * @param AW_Sarp2_Model_Profile $p
     *
     * @return array
     */
    protected function _importDataForValidation(AW_Sarp2_Model_Profile $p)
    {
        $data = array();
        foreach ($this->_mapRestrictionsToProfile as $restrictionsKey => $profileKey) {
            $levels = explode('/', $restrictionsKey);
            $currentData = &$data;
            foreach ($levels as $key) {
                if (!isset($currentData[$key])) {
                    $currentData[$key] = array();
                }
                $currentData = &$currentData[$key];
            }

            $currentData = $p->getData($profileKey);
        }

        if (isset($data['shipping_address'])) {
            $address = Mage::getModel('customer/address');
            $address->setData($data['shipping_address']);
            $data['shipping_address'] = $address;
        }

        if (isset($data['billing_address'])) {
            $address = Mage::getModel('customer/address');
            $address->setData($data['billing_address']);
            $data['billing_address'] = $address;
        }
        if (isset($data['payment_info'])) {
            $payment = Mage::getModel('payment/info');
            $payment->setData($data['payment_info']);
            $data['payment_info'] = $payment;
        }
        if (isset($data['customer'])) {
            $customer = Mage::getModel('customer/customer')->load($p->getData('customer_id'));
            if (!$customer->getId()) {
                $customer->setData(
                    array(
                        'email'      => isset($data['billing_address']['email'])
                            ? $data['billing_address']['email'] : null,
                        'prefix'     => isset($data['billing_address']['prefix'])
                            ? $data['billing_address']['prefix'] : null,
                        'firstname'  => isset($data['billing_address']['firstname'])
                            ? $data['billing_address']['firstname'] : null,
                        'middlename' => isset($data['billing_address']['middlename'])
                            ? $data['billing_address']['middlename'] : null,
                        'lastname'   => isset($data['billing_address']['lastname'])
                            ? $data['billing_address']['lastname'] : null,
                        'suffix'     => isset($data['billing_address']['suffix'])
                            ? $data['billing_address']['suffix'] : null,
                        'company'    => isset($data['billing_address']['company'])
                            ? $data['billing_address']['company'] : null,
                    )
                );
            }
            $data['customer'] = $customer;

            if(!isset($data['action'])) {
                $data['action']  = 1;
            }
        }

        if (array_key_exists('recurring_startdate', $data)) {
            $subscription = Mage::getModel('aw_sarp2/subscription')->setData(
                $p->getData('details/subscription/general')
            );
            $data['recurring_startdate'] = Mage::helper('aw_sarp2/subscription')->calculateSubscriptionStartDateForSelectedDate(
                $subscription, $data['recurring_startdate']
            );
        }

        if(isset($data['status'])) {
            if($data['status'] =='Successful') {
                $data['status'] = 'Active';
            }else {
                $data['status'] = 'Cancelled';
            }
        }

        if (isset($data['total_cycles'])) {
            $data['total_cycles'] = (int)$data['total_cycles'];
            if ($p->getData('details/subscription/type/period_is_infinite')) {
                $data['total_cycles'] = 0;
            }
        }

        if (isset($data['trial_intervaltype'])) {
            $data['trial_intervaltype'] = (int)$data['trial_intervaltype'];
            if (!$p->getData('details/subscription/type/trial_is_enabled')) {
                unset($data['trial_intervaltype']);
            }
        }
        if (isset($data['trial_amount'])) {
            $data['trial_amount'] = (float)$data['trial_amount'];
            if (!$p->getData('details/subscription/type/trial_is_enabled')) {
                unset($data['trial_amount']);
            }
        }

        return $data;
    }

    protected function _importDataForService(AW_Sarp2_Model_Profile $p)
    {
        $restrictionsData = $this->_importDataForValidation($p);
        $data = array();

        foreach ($this->_mapRestrictionsToService as $restrictionsKey => $serviceKey) {
            $levels = explode('/', $restrictionsKey);
            $value = isset($restrictionsData[$levels[0]]) ? $restrictionsData[$levels[0]] : null;

            unset($levels[0]);
            foreach ($levels as $key) {
                if (is_object($value)) {
                    $methodName = "get";
                    foreach (explode('_', $key) as $namePart) {
                        $methodName .= uc_words($namePart);
                    }
                    if (method_exists($value, $methodName)) {
                        $value = call_user_func(array($value, $methodName));
                    } else {
                        $value = $value->getData($key);
                    }
                } elseif (is_array($value)) {
                    $value = $value[$key];
                } else {
                    $value = null;
                }
            }
            if (!is_null($value)) {

                $data[$restrictionsKey] = $value;
            }
        }

        $subcribtion =$subscription = Mage::getModel('aw_sarp2/subscription')->setData(
            $p->getData('details/subscription/general')
        );



        //amount in decimal
        $data['recurring_startdate'] = date('m-d-Y', strtotime(
            Mage::helper('aw_sarp2/subscription')->calculateSubscriptionStartDateForSelectedDate(
            $subscription, $data['start_date']
        )
        ));
        if($subscription->getStartDateCode() ==  AW_Sarp2_Model_Source_Subscription_Startdate::MOMENT_OF_PURCHASE_CODE){
            $hackDateArray = explode('-', $data['recurring_startdate']);
            $unixHack = strtotime($hackDateArray[1].'-'.$hackDateArray[0].'-'.$hackDateArray[2]);

            $newHackDate = strtotime( "+1 day", $unixHack);
            $data['recurring_startdate'] =  date('m-d-Y',$newHackDate);
        }

        $data['recurring_intervaltype'] = $p->getData('details/subscription/type/period_unit') == 'days'?'DAY':'MNTH';
        $data['recurring_flag'] = 1;
        $data['recurring_auto'] = 1;

        if($p->getData('details/subscription/type/period_length')>1) {
            $data['recurring_interval'] = $p->getData('details/subscription/type/period_length');
            if( $data['recurring_intervaltype'] =='DAY') {
                $data['recurring_intervaltype'] ='CUSTDAY';
            }

            if( $data['recurring_intervaltype'] =='MNTH') {
                $data['recurring_intervaltype'] ='CUSTMNTH';
            }
        }

       // $data['recurring_number'] = $p->getData('details/subscription/type/trial_number_of_occurrences');
        if( $p->getData('details/subscription/type/period_is_infinite')==1) {
            $data['recurring_number'] = 999999;
        }else {

            $data['recurring_number'] =  $p->getData('details/subscription/type/period_number_of_occurrences');
        }
        $amount = Mage::getModel('directory/currency')->format(
            $p->getData('amount'),
            array('display'=>Zend_Currency::NO_SYMBOL),
            false
        );

        if($p->getData('details/subscription/type/trial_is_enabled')==1) {
            $data['trial_transactiontype'] = 1;
            $data['trial_amount'] = (float)$p->getData('details/subscription/item/trial_price');
            $data['trial_number'] = $p->getData('details/subscription/type/trial_number_of_occurrences');
            $data['trial_startdate'] = $data['recurring_startdate'];
            $nextStartingDate = $data['trial_number']+1;
            $trialStartDateArray = explode('-', $data['trial_startdate']);
            $unixTrial = strtotime($trialStartDateArray[1].'-'.$trialStartDateArray[0].'-'.$trialStartDateArray[2]);
            if( $data['recurring_intervaltype'] =='CUSTMNTH' ||  $data['recurring_intervaltype'] =='MNTH'){
                $newStartDate = strtotime( "+{$nextStartingDate} Month", $unixTrial);
            }else {
                $newStartDate = strtotime(" +{$nextStartingDate} day", $unixTrial);
            }
            if( $data['recurring_intervaltype'] =='CUSTMNTH') {
                $data['trial_intervaltype'] = 'MNTH';
            }elseif (  $data['recurring_intervaltype'] =='CUSTDAY') {
                $data['trial_intervaltype'] = 'DAY';
            }else {
                $data['trial_intervaltype'] = $data['recurring_intervaltype'];
            }


            $data['recurring_startdate'] =  date('m-d-Y',$newStartDate);
        }


        $data['recurring_amount'] =  $amount;
        $data['recurring_transactiontype'] =1;

        unset($data['start_date']);
        return $data;
    }

    /**
     * @param AW_Sarp2_Model_Profile $p
     *
     * @throws Mage_Core_Exception
     * @return array
     */
    public function getRecurringProfileDetails(AW_Sarp2_Model_Profile $p)
    {
        $this->_initConfigData($p);
        try {
            $response = $this->_service->getSubscriptionStatus($p->getReferenceId());

            return $this->_importDataFromServiceToProfile($response);
        } catch (Exception $e) {
            Mage::logException($e);
            throw new Mage_Core_Exception('Unable get profile details from Checkout.com');
        }
    }

    protected function _importDataFromServiceToProfile($data)
    {


        if($data['status']=='Successful') {
            $data['status']='Active';
        }

        return $data;
    }


}
