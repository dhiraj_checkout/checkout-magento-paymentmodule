<?php
class CheckoutAPI_PaymentModule_Model_Engine_PaymentModule_Service
{
    const PAYMENTMODULE_API_URL_UAT = 'https://api.checkout.com/Process/gateway.aspx';
    const PAYMENTMODULE_API_URL_LIVE = 'https://api.checkout.com/Process/gateway.aspx';
    const PAYMENTMODULE_API_QUERY = 'https://api.checkout.com/Process/recurringoperation.ashx';
    const PAYMENTMODULE_TRANS_TYPE_SALE = '1';
    const PAYMENT_INFO_CUSTOMER_TOKEN    = 'checkoutAPI_customer_token';
    const PAYMENT_INFO_TRANID    = 'checkoutAPI_tranId';
    const PAYMENT_INFO_TRACKID    = 'checkoutAPI_trackid';

    const DO_CREATE_SUBSCRIPTION     = 'CheckoutAPICreateSubscriptionRequest';
    const DO_UPDATE_SUBSCRIPTION     = 'CheckoutAPIUpdateSubscriptionRequest';
    const DO_CANCEL_SUBSCRIPTION     = 'CheckoutAPICancelSubscriptionRequest';
    const DO_GET_SUBSCRIPTION_STATUS = 'CheckoutAPIGetSubscriptionStatusRequest';

    protected $_client;
    protected $_config = null;
    protected $_serviceRequestOptionalFieldsMap = array (
        'CheckoutAPICreateSubscriptionRequest'=>array()
    );

    protected $_serviceResponseFieldsMap = array(
        'CheckoutAPICreateSubscriptionRequest'    => array(
            'customer_token',
            'shopper_profile',
            'tranid',
            'recurring_token',
            'result',
            'recurringstatuscode'
        ),
        'CheckoutAPIGetSubscriptionStatusRequest'    => array(
            'customer_token',
            'shopper_profile',
            'tranid',
            'recurring_token'
        ),
        'ARBCancelSubscriptionRequest'    => array(

        ),
        'ARBGetSubscriptionStatusRequest' => array(
            'status',
        ),
    );

    protected $_serviceResponseEachCallFields = array(
        'customer_token',
        'shopper_profile',
        'tranid',
        'recurring_token;',
    );

    function __construct()
    {
        $this->_client = new Varien_Object();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $this->_client->setHandler($ch);
    }

    /**
     * @param array $data
     */
    public function setConfigData($data)
    {
        $this->_config = $data;
    }

    public function createSubscription($data)
    {

        $this->_setRequestData($data);
        return $this->_runRequest(self::DO_CREATE_SUBSCRIPTION);
    }

    protected function _runRequest($command)
    {
        $url = self::PAYMENTMODULE_API_URL_LIVE;

        if (isset($this->_config['test_mode']) && $this->_config['test_mode']) {
            $url =  self::PAYMENTMODULE_API_URL_UAT;
        }

        curl_setopt($this->_client->getHandler(), CURLOPT_URL, $url);

        if (!isset($this->_config['username']) || !isset($this->_config['password'])) {
            throw new Exception('Some authenticate config fields are not specified here');
        }

        $requestData = $this->_client->getData('request');

        $requestXml = $this->_buildRequest($command);

        curl_setopt($this->_client->getHandler(), CURLOPT_POSTFIELDS, $requestXml);
        $httpResponse = curl_exec($this->_client->getHandler());
        if (!$httpResponse) {
            $errorString = "Request failed: "
                . curl_error($this->_client->getHandler())
                . " (" . curl_errno($this->_client->getHandler()) . ")";
            throw new Exception($errorString);
        }

        $response = $this->_parseResponse($httpResponse);

        if (!$this->_isCallSuccessful($response)) {
            /**
             * @throw exception
             */

            $this->_handleCallErrors($response);
        }

        $approveResponse = array_merge(
            array_intersect_key($response, array_flip($this->_serviceResponseFieldsMap[$command])),
            array_intersect_key($response, array_flip($this->_serviceResponseEachCallFields))
        );

        return $approveResponse;


    }

    public function _buildRequest($command)
    {

        $xml
            = '<?xml version="1.0" encoding="UTF-8"?>' .
            '<request>' .
            '<merchantid>' .$this->_config['username'].   '</merchantid>' .
            '<password>' . $this->_config['password'] . '</password>'.
            '<action>1</action>'.

            $this->_toXml(null, $this->_client->getData('request')) .
            '</request>';

        return $xml;

    }

    protected function _toXml($fieldName, $fieldValue)
    {
        $xml = '';
        if ($fieldName) {
            $xml .= "<{$fieldName}>";
        }
        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $xml .= $this->_toXml($key, $value);
            }
        } else {
            $xml .= $fieldValue;
        }
        if ($fieldName) {
            $xml .= "</{$fieldName}>";
        }

        return $xml;
    }
    protected function _setRequestData($data)
    {
        $this->_client->setData('request', $data);
    }

    public function getRecurringPaymentsProfileDetails($profileId)
    {
        $this->_setRequestData(
            array(
                'PROFILEID' => $profileId
            )
        );
        return $this->_runRequest(self::DO_GET_RECURRING_PAYMENTS_PROFILE_DETAILS);
    }


    private function _parseResponse($httpResponse)
    {
        // convert xml to array
        $httpResponse = preg_replace('/ xmlns:xsi[^>]+/', '', $httpResponse);
        $simpleXmlResponse = new Varien_Simplexml_Element($httpResponse);
        return $simpleXmlResponse->asArray();
    }

    private function _isCallSuccessful($response)
    {


        if (!isset($response['recurringstatuscode'])) {
            return false;
        }



        if ($response['recurringstatuscode'] == '1' || ($response['recurringstatuscode'] == '0')
            || ($response['recurringstatuscode'] == '3')) {

            return true;
        }

        return false;
    }

    private function _isCallSuccessfulStatus($response)
    {

       if(isset($response['type']) && $response['type']=='error') {
           return false;
       }

        return true;
    }

    private function _handleCallErrors($response)
    {


            throw new AW_Sarp2_Model_Engine_Authorizenet_Service_Exception('Payment was unsucessfull. Please verify card details or try with another card');

    }

    private function _handleCallErrorsStatus($response)
    {

            throw new AW_Sarp2_Model_Engine_Authorizenet_Service_Exception('Payment was unsucessfull. Please verify card details or try with another card');

    }

    public function getSubscriptionStatus($subscriptionId)
    {

        $this->_setRequestData(array('recurring_token' => $subscriptionId));
        return $this->_runRequestStatus(self::DO_GET_SUBSCRIPTION_STATUS);
    }


    protected function _runRequestStatus($command)
    {
        $url = self::PAYMENTMODULE_API_QUERY;

        curl_setopt($this->_client->getHandler(), CURLOPT_URL, $url);

        if (!isset($this->_config['username']) || !isset($this->_config['password'])) {
            throw new Exception('Some authenticate config fields are not specified here');
        }

        $requestData = $this->_client->getData('request');

        $requestXml = $this->_buildRequestStatus($command);
        curl_setopt($this->_client->getHandler(), CURLOPT_POSTFIELDS, $requestXml);
        $httpResponse = curl_exec($this->_client->getHandler());
        if (!$httpResponse) {
            $errorString = "Request failed: "
                . curl_error($this->_client->getHandler())
                . " (" . curl_errno($this->_client->getHandler()) . ")";
            throw new Exception($errorString);
        }

        $response = $this->_parseResponse($httpResponse);

        if (!$this->_isCallSuccessfulStatus($response)) {
            /**
             * @throw exception
             */
            $this->_handleCallErrorsStatus($response);
        }

        $approveResponse = array(
                            'shopper_profile' =>$response['shopper']['shopper_profile'],
                            'customer_token' =>$response['shopper']['customer_token'],
                            'recurring_token' =>$response['profiles']['profile']['recurring_token'],
                            'recurring_token' =>$response['profiles']['profile']['recurring_token'],
                       //     'tranid' => $response['profiles']['profile']['transactions']['transaction']['tranid'],
                        //    'transactiondate' => $response['profiles']['profile']['transactions']['transaction']['transactiondate'],
                             'status'=>$response['profiles']['profile']['recurring']['recurring_status']
                            );


        return $approveResponse;


    }

    public function _buildRequestStatus($command)
    {

        $xml
            = '<?xml version="1.0" encoding="UTF-8"?>' .
            '<request>' .
            '<merchantid>' .$this->_config['username'].   '</merchantid>' .
            '<password>' . $this->_config['password'] . '</password>'.
            '<operation>
               <mode>Query</mode>
            </operation><profile transactionlist="true" >'.

            $this->_toXml(null,$this->_client->getData('request')) .
            '</profile></request>';

        return $xml;

    }
}