<?php

class CheckoutAPI_PaymentModule_Model_Source_PaymentAction
{
    public function toOptionArray()
    {
        return array(
		    array(
                'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE,
                'label' => Mage::helper('paymentmodule')->__('Authorize Only')
            ),
            array(
                'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE,
                'label' => Mage::helper('paymentmodule')->__('Authorize and Capture')
            ),
        );
    }
}