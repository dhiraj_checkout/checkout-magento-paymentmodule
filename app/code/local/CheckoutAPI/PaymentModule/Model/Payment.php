<?php

class CheckoutAPI_PaymentModule_Model_Payment extends Mage_Payment_Model_Method_Cc
{
    protected $_code  = 'paymentmodule';

	const PAYMENTMODULE_API_URL_UAT = 'https://api.checkout.com/Process/gateway.aspx';
	const PAYMENTMODULE_API_URL_LIVE = 'https://api.checkout.com/Process/gateway.aspx';
	const PAYMENTMODULE_TRANS_TYPE_SALE = '1';
    const PAYMENT_INFO_CUSTOMER_TOKEN    = 'checkoutAPI_customer_token';
    const PAYMENT_INFO_TRANID    = 'checkoutAPI_tranId';
    const PAYMENT_INFO_TRACKID    = 'checkoutAPI_trackid';


    /**
     * Availability options
     */
    protected $_isGateway               = true;
    protected $_canAuthorize            = false;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = true;
    protected $_canSaveCc				= false;
	protected $_isInitializeNeeded      = false;

	protected $_allowCurrencyCode = array('USD');

	protected $_formBlockType = 'paymentmodule/form_payform';

	private $errorDetails;

	/**
     * Check method for processing with base currency
     *
     * @param string $currencyCode
     * @return boolean
     */
    public function canUseForCurrency($currencyCode)
    {
        if (!in_array($currencyCode, $this->_allowCurrencyCode)) {
            return false;
        }
        return true;
    }

	private function getApiUrl()
    {
        if (!$this->getConfigData('gateway_url')) {
            return self::PAYMENTMODULE_API_URL;
        }
        return $this->getConfigData('gateway_url');
    }
	private function getHeaders()
    {
        return array("MIME-Version: 1.0", "Content-type: application/x-www-form-urlencoded", "Contenttransfer-encoding: text");
    }
	private function getNVPArray(Varien_Object $payment, $action)
    {
		$order = $payment->getOrder();
		$billing = $payment->getOrder()->getBillingAddress();
		$shipping = $order->getShippingAddress();



			   $post_XML= '<request>' .
					'<merchantid>' . Mage::helper('core')->decrypt($this->getConfigData('user_name')) . '</merchantid>' .
					'<password>' . Mage::helper('core')->decrypt($this->getConfigData('password')) . '</password>' .
					'<action>'. $action . '</action>' .
					'<trackid>' . $order->getIncrementId() . '</trackid>' .
					'<bill_currencycode>' .  $order->getBaseCurrencyCode() . '</bill_currencycode>' .
					'<bill_cardholder>' . $payment->getCcOwner() . '</bill_cardholder>' .
					'<bill_cc_type>CC</bill_cc_type>' .
					'<bill_cc_brand></bill_cc_brand>' .
					'<bill_cc>' . $payment->getCcNumber() . '</bill_cc>' .
					'<bill_expmonth>' . $payment->getCcExpMonth() . '</bill_expmonth>' .
					'<bill_expyear>' . $payment->getCcExpYear() . '</bill_expyear>' .
					'<bill_cvv2>' . $payment->getCcCid() . '</bill_cvv2>' .
					'<bill_address>' . $billing->getStreet(1) . '</bill_address>' .
					'<bill_address2></bill_address2>' .
					'<bill_postal>' . $billing->getPostcode() . '</bill_postal>' .
					'<bill_city>' . $billing->getCity() . '</bill_city>' .
					'<bill_state>' . $billing->getRegion() . '</bill_state>' .
					'<bill_email>' . $order->getCustomerEmail() . '</bill_email>' .
					'<bill_country>' . $billing->getCountry() . '</bill_country>' .
					'<bill_amount>' . $payment->getAmount() . '</bill_amount>' .
					'<bill_phone>' . $billing->getTelephone() . '</bill_phone>' .
					'<bill_fax>' . '' . '</bill_fax>' .
					'<merchantcustomerid>' . $billing->getCustomerId() . '</merchantcustomerid>' .
					'<bill_customerip>' . Mage::helper('core/http')->getRemoteAddr(true) . '</bill_customerip>' .
					'<bill_merchantip>' . $order->getRemoteIp() . '</bill_merchantip>';
					if (!empty($shipping))
					{
					 $post_XML =  $post_XML .'<ship_address>' .  $shipping->getStreet(1) . '</ship_address>' .
								'<ship_email>' . '' . '</ship_email>' .
								'<ship_postal>' .  $shipping->getPostcode() . '</ship_postal>' .
								'<ship_address2></ship_address2>' .
								'<ship_type>' . '' . '</ship_type>' .
								'<ship_city>' . $shipping->getCity() . '</ship_city>' .
								'<ship_state>' . $shipping->getRegion() . '</ship_state>' .
								'<ship_phone>' . '' . '</ship_phone>' .
								'<ship_country>' . $shipping->getCountry() . '</ship_country>' .
								'<ship_fax>' . '' . '</ship_fax>' ;
					}
					$post_XML =  $post_XML . '<udf1></udf1>' .
								'<udf2></udf2>' .
								'<udf3></udf3>' .
								'<udf4></udf4>' .
								'<udf5></udf5>' .
								'</request>';


		return  $post_XML;

    }

	private function processPayment($npvStr) {



		if($this->getConfigData('test') == '1')
		{
			$gateway_url=self::PAYMENTMODULE_API_URL_UAT;
		}else
		{
			$gateway_url=self::PAYMENTMODULE_API_URL_LIVE;
		}



		$http = new Varien_Http_Adapter_Curl();
        $config = array('timeout' => 60);
		$http->setConfig($config);


		$http->write(Zend_Http_Client::POST, $gateway_url, '1.1', array(), $npvStr);
		$response = $http->read();




        if ($http->getErrno()) {
            $http->close();
            $this->errorDetails = array(
				'type' => 'CURL',
				'code' => $http->getErrno(),
				'message' => $http->getError()
			);
			return false;
        }
        $http->close();

		$response = preg_split('/^\r?$/m', $response, 2);
        $response = trim($response[1]);

		return($response);
	}

	/**
     * this method is called if we are just authorising
     * a transaction
     */
    public function authorize (Varien_Object $payment, $amount)
    {
		$error = false;
		$payment->setAmount($amount);

		$nvpStr = $this->getNVPArray($payment, '4');


		$response = $this->processPayment($nvpStr);



		if (!$response) {
			$error = Mage::helper('paymentmodule')->__('Gateway request error: %s', $this->errorDetails['message']);
		}
		else {
			$result = @simplexml_load_string($response);
			$gwerror = $result->{'error_text'};

			if(!$gwerror){
				if (!$result) {
					$error = Mage::helper('paymentmodule')->__('Cannot process your payment. Please try again.');
				}
				elseif ($result->{'responsecode'} != '0') {
					$error = Mage::helper('paymentmodule')->__("Cannot process your payment, error: %s (%s). Please try again.", $result->{'result'},$result->{'responsecode'}, $response);
				}
			}else
			{
				$error = Mage::helper('paymentmodule')->__("Cannot process your payment, error: %s. Please try again.", $gwerror, $response);
			}
		}
		if ($error !== false) {
            Mage::throwException($error);
        }
		else {
			$payment->setStatus(self::STATUS_APPROVED);
			$payment->setLastTransId($result->{'tranid'});
            $payment->setAdditionalInformation(self::PAYMENT_INFO_CUSTOMER_TOKEN, $result->{'customer_token'});
            $payment->setAdditionalInformation(self::PAYMENT_INFO_TRANID, $result->{'customer_token'})
                ->setAdditionalInformation(self::PAYMENT_INFO_TRACKID,$result->{'trackid'});



		}

		return $this;
    }

    /**
     * this method is called if we are authorising AND
     * capturing a transaction
     */
    public function capture (Varien_Object $payment, $amount)
    {


		$error = false;
		$payment->setAmount($amount);

		$nvpStr = $this->getNVPArray($payment, '1');


		$response = $this->processPayment($nvpStr);



		if (!$response) {
			$error = Mage::helper('paymentmodule')->__('Gateway request error: %s', $this->errorDetails['message']);
		}
		else {
			$result = @simplexml_load_string($response);
			$gwerror = $result->{'error_text'};

			if(!$gwerror){
				if (!$result) {
					$error = Mage::helper('paymentmodule')->__('Cannot process your payment. Please try again.');
				}
				elseif ($result->{'responsecode'} != '0') {
					$error = Mage::helper('paymentmodule')->__("Cannot process your payment, error: %s (%s). Please try again.", $result->{'result'},$result->{'responsecode'}, $response);
				}
			}else
			{
				$error = Mage::helper('paymentmodule')->__("Cannot process your payment, error: %s. Please try again.", $gwerror, $response);
			}
		}
		if ($error !== false) {
            Mage::throwException($error);

        }
		else {

			$payment->setStatus(self::STATUS_APPROVED);
			$payment->setLastTransId( (string)$result->{'tranid'});
            $payment->setAdditionalInformation(self::PAYMENT_INFO_CUSTOMER_TOKEN, (string)$result->{'customer_token'});
            $payment->setAdditionalInformation(self::PAYMENT_INFO_TRANID,  (string)$result->{'customer_token'})
                ->setAdditionalInformation(self::PAYMENT_INFO_TRACKID, (string)$result->{'trackid'});
		}

		return $this;
    }

    /**
     * called if refunding
     */
    public function refund (Varien_Object $payment, $amount)
    {
		Mage::throwException("Refund not Supported.");
		return $this;
    }

    /**
     * called if voiding a payment
     */
    public function void (Varien_Object $payment)
    {
		Mage::throwException("Void not Supported.");
		return $this;
    }

    public function isApplicableToQuote($quote, $checksBitMask)
    {
        if(Mage::helper('core')->isModuleEnabled('AW_Sarp2')){

            if (
                $checksBitMask & Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL
                && Mage::helper('aw_sarp2/quote')->isQuoteHasSubscriptionProduct($quote)
            ) {
                $checksBitMask -= Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL;
            }
        }

        return parent::isApplicableToQuote($quote, $checksBitMask);
    }
}