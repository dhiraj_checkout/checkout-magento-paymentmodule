<?php
class CheckoutAPI_PaymentModule_Helper_Engine extends AW_Sarp2_Helper_Engine
{
    protected function _initEngineConfig()
    {

        Mage::dispatchEvent('subscription_engine_initEngineConfig_before',
        array('engine' => $this->_enginesConfig));
        parent::_initEngineConfig();
        $classParent = preg_replace('/_Helper_.*$/','',get_parent_class($this));

        $configFilePath = Mage::getModuleDir(self::ENGINE_CONFIGURATION_FOLDER,$classParent)
            . DS . self::ENGINE_CONFIGURATION_FILE;

        $config = new Varien_Simplexml_Config($configFilePath);
        $parentEngine = $config->getNode()->asArray();
        $this->_enginesConfig = array_merge($parentEngine,$this->_enginesConfig);

        Mage::dispatchEvent('subscription_engine_initEngineConfig_after',
            array('engine' => $this->_enginesConfig));
    }
}